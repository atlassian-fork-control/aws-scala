package io.atlassian.aws.dynamodb

import io.atlassian.aws.dynamodb.DynamoDB.{ ReadConsistency, SelectAttributes }

trait Scans {
  // Clients are not required to choose K to be a composite of H and R. As a result, we'll handle the pagination keys
  // tuples.
  type H
  type R

  sealed trait Scan {
    val config: Scan.Config
    def config(c: Scan.Config): Scan
    def from(exclusiveStateKey: (H, R)): Scan = config(config.copy(exclusiveStartKey = Some(exclusiveStateKey)))
  }

  object Scan {
    def table(config: Config = Config()): Scan =
      Table(config)

    case class Table(config: Config) extends Scan {
      def config(c: Scan.Config): Table = copy(config = c)
    }

    case class Config(select: Option[SelectAttributes] = None, limit: Option[Int] = None, consistency: ReadConsistency = ReadConsistency.Eventual, exclusiveStartKey: Option[(H, R)] = None)
  }
}
